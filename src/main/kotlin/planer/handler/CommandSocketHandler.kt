package planer.handler

import com.fasterxml.jackson.module.kotlin.readValue
import io.javalin.Javalin
import io.javalin.Javalin.log
import io.javalin.websocket.WsContext
import org.jetbrains.exposed.exceptions.ExposedSQLException
import planer.dto.CmdName
import planer.dto.Command
import planer.dto.ErrorCode
import planer.dto.addError
import planer.dto.views.NoteView
import planer.globalObjectMapper
import planer.dao.NoteDao
import java.util.concurrent.ConcurrentHashMap

class CommandSocketHandler {

    private val noteRepository = NoteDao()
    private val connectionUserMap = ConcurrentHashMap<WsContext, Int>()
    private var nextUserNumber = 1

    fun addHandler(connect: Javalin) =
        connect.ws("/endpoint") { ws ->
            ws.onConnect { ctx ->
                nextUserNumber++.also {
                    log.info("user $it was connected")
                    connectionUserMap[ctx] = it
                }
                withNoteView(noteRepository.getOrderedAll()).also {
                    ctx.send(it)
                    log.info("sent all notes: $it")
                }
            }
            ws.onClose {
                log.info("user ${connectionUserMap[it]} is gone")
                connectionUserMap.remove(it)
            }
            ws.onMessage { ctx ->
                log.info("user ${connectionUserMap[ctx]} send command")
                log.info("income command: ${ctx.message()}")
                try {
                    runAndCallback(globalObjectMapper.readValue(ctx.message())).apply {
                        val commandStr = withNoteView(this)
                        if (CmdName.valueOf(this.cmd) == CmdName.ERROR) {
                            ctx.send(commandStr)
                        } else {
                            connectionUserMap.keys
                                .filter { it.session.isOpen }
                                .forEach { it.send(commandStr) }
                        }
                        log.info("outcome command: $commandStr")
                    }
                } catch (ex: ExposedSQLException) {
                    ctx.send(withNoteView(Command().addError(ErrorCode.INTERNAL)))
                }
            }
            ws.onError {
                log.error("Error detected [sessionId = ${it.sessionId}], [session = ${it.session}]: ${it.error().toString()}")
            }
        }!!

    private fun runAndCallback(cmd: Command) = when (CmdName.valueOf(cmd.cmd)) {
        CmdName.GET_ALL -> noteRepository.getOrderedAll()
        CmdName.CREATE -> noteRepository.create(cmd.note!!)
        CmdName.UPDATE -> noteRepository.update(cmd.note!!)
        CmdName.TOGGLE -> noteRepository.toggle(cmd.note!!)
        CmdName.DELETE -> noteRepository.delete(cmd.id!!)
        CmdName.DELETE_ALL -> noteRepository.deleteAll()
        else -> {
            log.warn("Command ${cmd.cmd} not exist")
            Command().addError(ErrorCode.BAD_REQUEST)
        }
    }

    private fun withNoteView(obj: Any) = globalObjectMapper
        .writerWithView(NoteView.ToFront::class.java)
        .writeValueAsString(obj)

}
