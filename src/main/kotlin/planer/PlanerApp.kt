package planer

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.javalin.Javalin
import io.javalin.core.JavalinConfig
import io.javalin.plugin.json.JavalinJackson
import io.javalin.plugin.openapi.annotations.ContentType
import org.jetbrains.exposed.sql.Database
import planer.handler.CommandSocketHandler
import planer.dao.NoteDao


fun startServer(): Javalin = Javalin
    .create { initialConfig(it) }
    .start(8000)

fun initialConfig(config: JavalinConfig) = config.apply {
    wsFactoryConfig { wsFactory ->
        wsFactory.policy.maxTextMessageSize = 10000
        wsFactory.policy.idleTimeout = 100_000_000
    }
    defaultContentType = ContentType.JSON
    JavalinJackson.configure(globalObjectMapper)
    enableDevLogging()
}

val globalObjectMapper = jacksonObjectMapper().apply {
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

fun main() {

    Database.connect(
        url = "jdbc:postgresql://localhost:5432/postgres",
        driver = "org.postgresql.Driver",
        user = "postgres",
        password = "superpass"
    )

    NoteDao().init()

    val connect = startServer()
    CommandSocketHandler().addHandler(connect)

}
